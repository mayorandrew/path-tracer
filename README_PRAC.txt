===== IV  Прак =====
= Андрей Старостин =
==== Группа 320 ====
===== ВМК  МГУ =====
======= 2014 =======



[!] Внимание! 

С целью уменьшения размера архива библиотеки зависимостей (см. список в конце readme), необходимые для компиляции, не приложены.
Полный архив расположен по адресу: https://www.dropbox.com/s/lumho0by98os4lo/path-tracer.zip?dl=0
В полном архиве так же находится файлы сцен scene.max и scene2.max



Искомая строчка находится в файле
PathTracer/src/tracer/Renderer.cpp:64

Запустить рендеринг можно через
bin/x64/PathTracer.exe



См. полный README.txt

