#pragma once
#include "include/tracer/Scene.h"
#include <fstream>

class MyFormatEncoder
{
public:
	MyFormatEncoder();
	~MyFormatEncoder();

	void save(tracer::Scene &scene, const std::string &filename);

private:
	void saveCamera(tracer::Camera &camera);
	void saveShapes(tracer::Scene &scene);
	void saveShape(shape::Shape *shape);
	void saveNodes(tracer::SceneNode *node);
	void property(const std::string &name, bool newline = true);
	void startBlock(bool newline = false, bool addline = true);
	void endBlock(bool newline = true);
	void write(const std::string &message);
	void vector(glm::vec3 v);
	void vector(glm::vec2 v);

	std::ofstream output;
	int indent;
	std::map<shape::Shape const*, int> shapeRegistry;
};

