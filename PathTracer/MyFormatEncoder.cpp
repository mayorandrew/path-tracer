#include "MyFormatEncoder.h"
#include "include/shape/Mesh.h"
#include "include/tracer/ShapeSceneNode.h"

using namespace tracer;
using namespace std;
using namespace glm;
using namespace shape;

MyFormatEncoder::MyFormatEncoder() {
}


MyFormatEncoder::~MyFormatEncoder() {
}

void MyFormatEncoder::save(Scene &scene, const string &filename)
{
	output.open(filename);
	indent = 0;

	startBlock();
	
	property("Camera"); saveCamera(*scene.camera); output << ",\n";
	property("Shapes"); saveShapes(scene); output << ",\n";
	property("Nodes"); saveNodes(scene.root); output << ",\n";


	endBlock();
}

void MyFormatEncoder::saveCamera(Camera &camera)
{
	startBlock();
	property("AngleH");	output << camera.m_angleH * 180 / M_PI << ",\n";
	property("Up");	vector(camera.m_up); output << ",\n";
	property("Pos"); vector(camera.m_position); output << ",\n";
	property("Dir"); vector(camera.m_direction); output << ",\n";
	endBlock();
}

void MyFormatEncoder::saveShapes(Scene &scene)
{
	startBlock();

	int shapeNumber = 0;
	for (auto shape : scene.shapes)
	{
		shapeRegistry[shape] = shapeNumber;
		property(to_string(shapeNumber));
		saveShape(shape);
		output << ",\n";
		shapeNumber++;
	}

	endBlock();
}

void MyFormatEncoder::saveShape(Shape *shape)
{
	startBlock();
	Mesh *mesh = dynamic_cast<Mesh*>(shape);
	if (mesh)
	{
		property("_type"); output << "\"Mesh\",\n";
		
		property("Normals"); output << "[";
		for (auto v : mesh->normals) {
			vector(v); output << ", ";
		}
		output << "],\n";

		property("Tangents"); output << "[";
		for (auto v : mesh->tangents) {
			vector(v); output << ", ";
		}
		output << "],\n";

		property("BiTangents"); output << "[";
		for (auto v : mesh->bitangents) {
			vector(v); output << ", ";
		}
		output << "],\n";

		property("TexCoords"); output << "[";
		for (auto v : mesh->texCoords) {
			vector(v); output << ", ";
		}
		output << "],\n";

		property("Vertices"); output << "[";
		for (auto v : mesh->vertices) {
			vector(v); output << ", ";
		}
		output << "],\n";

		property("SubMeshes"); output << "[\n"; indent++;
		for (auto m : mesh->submeshes)
		{
			startBlock(true);

			property("Triangles");
			output << "[\n"; indent++;
			for (auto t : m.triangles)
			{
				startBlock(true, false);
				property("V", false); output << "[" << t.vertices[0] << "," << t.vertices[1] << "," << t.vertices[2] << "], ";
				property("N", false); output << "[" << t.normals[0] << "," << t.normals[1] << "," << t.normals[2] << "], ";
				property("TC", false); output << "[" << t.texCoords[0] << "," << t.texCoords[1] << "," << t.texCoords[2] << "], ";
				property("TN", false); output << "[" << t.tangents[0] << "," << t.tangents[1] << "," << t.tangents[2] << "], ";
				property("BT", false); output << "[" << t.bitangents[0] << "," << t.bitangents[1] << "," << t.bitangents[2] << "], ";
				endBlock(false);  output << ",\n";
			}
			indent--; write("],\n");

			endBlock();
			output << ",\n";
		}
		indent--; write("],\n");
	}
	endBlock();
}

void MyFormatEncoder::saveNodes(SceneNode *node)
{
	output << "[\n"; indent++;

	for (auto n : node->m_children)
	{
		startBlock(true);

		property("Matrix"); output << "[";
		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {
				output << n->m_relmodelmatrix[column][row] << ",";
			}
		}
		output << "],\n";
		ShapeSceneNode *nn = dynamic_cast<ShapeSceneNode*>(n);
		if (nn)
		{
			property("_type"); output << "\"ShapeSceneNode\",\n";
			property("Shape"); output << "\"" << shapeRegistry[&nn->m_shape] << "\",\n";
		} else
		{
			property("_type"); output << "\"SceneNode\",\n";
		}

		if (n->m_children.size() > 0)
		{
			property("Children"); saveNodes(n); output << "\n";
		}

		endBlock();
		output << ",\n";
	}

	indent--; write("]");
}

void MyFormatEncoder::vector(vec3 v) {
	output << "[" << v.x << "," << v.y << "," << v.z << "]";
}

void MyFormatEncoder::vector(vec2 v) {
	output << "[" << v.x << "," << v.y << "]";
}

void MyFormatEncoder::property(const string &name, bool newline)
{
	if (newline) {
		write("\"" + name + "\": ");
	} else
	{
		output << "\"" << name << "\": ";
	}
}

void MyFormatEncoder::startBlock(bool newline, bool addline) {
	if (newline) {
		if (addline) {
			write("{\n");
		} else {
			write("{ ");
		}
	} else {
		if (addline) {
			output << " {\n";
		} else {
			output << " { ";
		}
	}
	indent++;
}

void MyFormatEncoder::endBlock(bool newline) {
	indent--;
	if (newline) {
		write("}");
	} else {
		output << " }";
	}
}

void MyFormatEncoder::write(const std::string& message)
{
	for (int i = 0; i < indent; i++)
	{
		output << "\t";
	}
	output << message;
}
