#include "shape/Sphere.h"

#include <gtx/norm.hpp>

using namespace core;
using namespace tracer;
using namespace glm;

namespace shape
{
	Sphere::Sphere(float radius) : m_radius(radius), m_sqradius(radius * radius) {
		m_bbox = core::BBox(glm::vec3(-m_radius), glm::vec3(m_radius));
	}
	Sphere::~Sphere() {}

	bool Sphere::intersects(const Ray &ray, Intersection *intersect) const {
		vec3 vpc = -ray.pos();

		if (dot(vpc, ray.dir()) < 0) {
			float len = length2(vpc);
			if (len >= m_sqradius) {
				return false;
			} else {
				if (intersect) {
					vec3 pc = ray.dir() * dot(ray.dir(), vpc);
					float dist = sqrt(m_sqradius - length2(pc));
					float di1 = dist - length(pc);
					intersect->distance = di1;
					intersect->position = ray.pos() + ray.dir() * di1;
					intersect->normal = normalize(intersect->position);
					// bad solution for tangent space:
					intersect->tangent = normalize(core::anyPerpendicular(intersect->normal));
					intersect->bitangent = normalize(cross(intersect->tangent, intersect->normal));
					intersect->ray = ray;
				}
				return true;
			}
		} else {
			vec3 pc = ray.dir() * dot(ray.dir(), vpc);
			vec3 ppc = pc + ray.pos();
			if (length2(ppc) > m_sqradius) {
				return false;
			} else {
				if (intersect) {
					float dist = sqrt(m_sqradius - length2(ppc));
					float di1 = 0;
					if (length2(vpc) > m_sqradius) {
						di1 = length(pc) - dist;
					} else {
						di1 = length(pc) + dist;
					}
					intersect->distance = di1;
					intersect->position = ray.pos() + ray.dir() * di1;
					intersect->normal = normalize(intersect->position);
					intersect->tangent = normalize(core::anyPerpendicular(intersect->normal));
					intersect->bitangent = normalize(cross(intersect->tangent, intersect->normal));
					intersect->ray = ray;
				}
				return true;
			}
		}
	}
}