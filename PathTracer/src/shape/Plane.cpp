#include "shape/Plane.h"

#include <gtx/norm.hpp>
#include <math.h>

#include <iostream>

using namespace core;
using namespace tracer;
using namespace glm;

#define M_EPS 0.000006f

namespace shape
{
	Plane::Plane(vec3 pos, vec3 normal) : m_pos(pos), m_normal(normal) {}
	Plane::~Plane() {}

	bool Plane::intersects(const Ray &ray, Intersection *intersect) const {
		float dir_nor = dot(ray.dir(), m_normal);
		if (abs(dir_nor) < M_EPS) {
			return false;
		}
		vec3 delta = m_pos - ray.pos();
		float t = dot(delta, m_normal) / dir_nor;

		if (t > 0) {
			if (intersect) {
				vec3 pos = ray.pos() + ray.dir() * t;
				intersect->position = pos;
				intersect->normal = m_normal;
				intersect->tangent = normalize(core::anyPerpendicular(m_normal));
				intersect->bitangent = normalize(cross(intersect->tangent, intersect->normal));
				intersect->ray = ray;
				intersect->distance = t;
			}
			return true;
		} else {
			return false;
		}
	}
}