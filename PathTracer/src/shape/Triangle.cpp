#include "shape/Triangle.h"
#include "core/Exception.h"

using namespace core;
using namespace glm;

namespace shape
{
	bool Triangle::intersects(
		const std::vector<glm::vec3> &verticesData,
		const std::vector<glm::vec3> &normalsData,
		const std::vector<glm::vec2> &texCoordsData,
		const std::vector<glm::vec3> &tangentsData,
		const std::vector<glm::vec3> &bitangentsData,
		const core::Ray &ray, 
		tracer::Intersection *intersect) const {

		vec3 pos0 = verticesData[vertices[0]];
		vec3 pos1 = verticesData[vertices[1]];
		vec3 pos2 = verticesData[vertices[2]];

		vec3 edge1 = pos1 - pos0;
		vec3 edge2 = pos2 - pos0;

		/*if (dot(cross(edge1, edge2), ray.dir()) >= 0) {
			return false;
		}*/

		mat3 m(-ray.dir(), edge1, edge2);
		vec3 res = inverse(m) * (ray.pos() - pos0);

		float t = res.x;
		float u = res.y;
		float v = res.z;

		if (!(t >= 0 && u >= 0 && v >= 0 && u + v <= 1)) {
			return false;
		}

		if (intersect) {
			vec3 pos = ray.pos() + t * ray.dir();
			intersect->distance = t;
			intersect->position = pos;
			intersect->ray = ray;

			vec3 normal = normalsData[normals[0]] * (1 - u - v) + normalsData[normals[1]] * u + normalsData[normals[2]] * v;
			intersect->normal = normalize(normal);

			vec2 texCoord = texCoordsData[texCoords[0]] * (1 - u - v) + texCoordsData[texCoords[1]] * u + texCoordsData[texCoords[2]] * v;
			intersect->texCoord = texCoord;

			vec3 tangent = tangentsData[tangents[0]] * (1 - u - v) + tangentsData[tangents[1]] * u + tangentsData[tangents[2]] * v;
			if (length(tangent) < 0.1) {
				throw Exception("Division by zero");
			}
			intersect->tangent = normalize(tangent);

			vec3 bitangent = bitangentsData[bitangents[0]] * (1 - u - v) + bitangentsData[bitangents[1]] * u + bitangentsData[bitangents[2]] * v;
			intersect->bitangent = normalize(bitangent);
		}
		return true;
	}
}