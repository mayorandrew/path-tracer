#include "shape/Mesh.h"

using namespace tracer;
using namespace glm;

namespace shape
{
	SubMesh::SubMesh() {}
	SubMesh::~SubMesh() {}

	bool SubMesh::intersects(const Mesh &mesh, const core::Ray &ray, tracer::Intersection *intersect) const {
		if (intersect) {
			bool intersects = false;
			for (const Triangle &tr : triangles) {
				Intersection intersection;
				if (tr.intersects(mesh.vertices, mesh.normals, mesh.texCoords, mesh.tangents, mesh.bitangents, ray, &intersection)) {
					if (!intersects || intersection.distance < intersect->distance) {
						*intersect = intersection;
						intersects = true;
					}
				}
			}

			return intersects;
		} else {
			for (const Triangle &tr : triangles) {
				if (tr.intersects(mesh.vertices, mesh.normals, mesh.texCoords, mesh.tangents, mesh.bitangents, ray)) {
					return true;
				}
			}

			return false;
		}
	}

	void SubMesh::prepare(const Mesh &mesh) {
		for (const Triangle &tr : triangles) {
			for (int v : tr.vertices) {
				m_bbox.add(mesh.vertices[v]);
			}
		}
	}

	Mesh::Mesh() {}
	Mesh::~Mesh() {}

	bool Mesh::intersects(const core::Ray &ray, tracer::Intersection *intersect) const {
		if (intersect) {
			bool intersects = false;

			for (const SubMesh &submesh : submeshes) {
				if (submesh.getBBox().intersects(ray)) {
					Intersection intersection;
					if (submesh.intersects(*this, ray, &intersection)) {
						if (!intersects || intersection.distance < intersect->distance) {
							*intersect = intersection;
							intersects = true;
						}
					}
				}
			}

			return intersects;
		} else {
			for (const SubMesh &submesh : submeshes) {
				if (submesh.getBBox().intersects(ray) && submesh.intersects(*this, ray)) {
					return true;
				}
			}

			return false;
		}
	}

	void Mesh::prepare() {
		for (SubMesh &submesh : submeshes) {
			submesh.prepare(*this);
			m_bbox.add(submesh.getBBox());
		}
	}
}