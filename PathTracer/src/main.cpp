#include "core/Platform.h"
#include "core/Exception.h"
#include "tracer/Renderer.h"
#include "tracer/FastRenderer.h"
#include "tracer/ShapeSceneNode.h"
#include "shape/Sphere.h"
#include "shape/Plane.h"
#include "shape/Mesh.h"
#include "decode/MeshDecoder.h"
#include "material/Lambert.h"
#include "material/Light.h"
#include "material/Mirror.h"
#include "core/Config.h"
#include "decode/AiDecoder.h"
#include <gtx/norm.hpp>

#include <iostream>
#include <time.h>
#include "../MyFormatEncoder.h"

using namespace std;
using namespace glm;
using namespace core;
using namespace shape;
using namespace tracer;
using namespace decode;

namespace core
{
	std::default_random_engine g_generator = std::default_random_engine();
}

TImage<vec4> blend(const TImage<vec4> &im1, const TImage<vec4> &im2) {
	TImage<vec4> n(im1);
	int size = im1.width() * im2.height();
#pragma omp parallel for
	for (int i = 0; i < size; i++) {
		vec4 t1 = im1[i];
		vec4 t2 = im2[i];
		vec3 res = (vec3(t1) * t1[3] + vec3(t2) * t2[3]) / (t1[3] + t2[3]);
		n[i] = vec4(res, t1[3] + t2[3]);
	}
	return n;
}

void average(Image &output, const Image &a, const Image &b) {
	int size = a.width() * b.height();
#pragma omp parallel for
	for (int i = 0; i < size; i++) {
		output[i] = packColor((unpackColor(a[i]) + unpackColor(b[i])) / 2);
	}
}

void variance(TImage<float> &difference, const Image &a, const Image &b) {
	int size = a.width() * b.height();
#pragma omp parallel for
	for (int i = 0; i < size; i++) {
		vec3 delta = vec3(unpackColor(a[i]) - unpackColor(b[i]));
		difference[i] = length2(delta) / 2;
	}
}

void variance(TImage<float> &difference, const TImage<float> &a, const TImage<float> &b) {
	int size = a.width() * b.height();
#pragma omp parallel for
	for (int i = 0; i < size; i++) {
		float delta = a[i] - b[i];
		difference[i] = (delta * delta) / 2;
	}
}

void relativeDifference(TImage<float> &difference, const TImage<Uint32> &a, const TImage<Uint32> &b) {
	int size = a.width() * b.height();
#pragma omp parallel for
	for (int i = 0; i < size; i++) {
		vec3 colA = vec3(unpackColor(a[i]));
		vec3 colB = vec3(unpackColor(b[i]));
		float delta2 = length2(colA - colB);
		difference[i] = glm::min(delta2 / (0.001f + glm::max(20.0f * 20.0f * 3, length2(colA))), 1.0f);
		difference[i] += glm::min(delta2 / (0.001f + glm::max(20.0f * 20.0f * 3, length2(colB))), 1.0f);
	}
}

void gauss(TImage<float> &result, const TImage<float> &im) {
	float sigma = 0.8;
	float sigma2 = sigma * sigma;
	int filterSize = 3 * sigma;
#pragma omp parallel for
	for (int y = 0; y < im.height(); y++) {
		for (int x = 0; x < im.width(); x++) {

			float res = 0;
			for (int dx = -filterSize; dx <= filterSize; dx++) {
				for (int dy = -filterSize; dy <= filterSize; dy++) {
					float weight = exp(-(dx * dx + dy * dy) / (2 * sigma2)) / (2 * M_PI * sigma2);
					res += weight * im.cpixel(x + dx, y + dy);
				}
			}
			result.pixel(x, y) = res;

		}
	}
}

void nlMeans(TImage<Uint32> &result, const TImage<Uint32> &image, const TImage<Uint32> &reference, const TImage<float> &var) {
	int filterSize = 1;
	int patchSize = 3;
	float k2 = 0.45 * 0.45;
	float alpha = 4;
	int width = image.width();
	int height = image.height();
	int offset = 0; // filterSize + patchSize;

#pragma omp parallel for
	for (int y = offset; y < height - offset; y++) {
		int yIndex = y * width;
		for (int x = offset; x < width - offset; x++) {
			vec3 res;
			float weights = 0;

			for (int dy = -filterSize; dy <= filterSize; dy++) {
				for (int dx = -filterSize; dx <= filterSize; dx++) {
					float patchDist = 0;

					for (int cy = -patchSize; cy <= patchSize; cy++) {
						for (int cx = -patchSize; cx <= patchSize; cx++) {
							ivec3 c1 = unpackColor(reference.cpixel(x + dx + cx, y + dy + cy));
							ivec3 c2 = unpackColor(reference.cpixel(x + cx, y + cy));
							vec3 delta = vec3(c1 - c2) / 255.0f;
							float varP = var.cpixel(x + cx, y + cy);
							float varQ = var.cpixel(x + dx + cx, y + dy + cy);
							patchDist += (length2(delta) - alpha * (varP + glm::min(varP, varQ))) / (1.0e-10 + k2 * (varP + varQ));
						}
					}

					patchDist /= 3.0;
					patchDist /= (2 * patchSize + 1) * (2 * patchSize + 1);

					float weight = exp(-glm::max(0.0f, patchDist));
					ivec3 pixel = unpackColor(image.cpixel(x + dx, y + dy));

					res += vec3(pixel) * weight;
					weights += weight;
				}
			}

			result.pixel(x, y) = packColor(ivec3(res / weights));
		}
	}
}

void nlMeans(TImage<float> &result, const TImage<float> &image, const TImage<float> &reference, const TImage<float> &var) {
	int filterSize = 1;
	int patchSize = 3;
	float k2 = 0.45 * 0.45;
	float alpha = 4;
	int width = image.width();
	int height = image.height();
	int offset = 0; // filterSize + patchSize;

#pragma omp parallel for
	for (int y = offset; y < height - offset; y++) {
		int yIndex = y * width;
		for (int x = offset; x < width - offset; x++) {
			float res = 0;
			float weights = 0;

			for (int dy = -filterSize; dy <= filterSize; dy++) {
				for (int dx = -filterSize; dx <= filterSize; dx++) {
					float patchDist = 0;

					for (int cy = -patchSize; cy <= patchSize; cy++) {
						for (int cx = -patchSize; cx <= patchSize; cx++) {
							float c1 = reference.cpixel(x + dx + cx, y + dy + cy);
							float c2 = reference.cpixel(x + cx, y + cy);
							float delta = (c1 - c2);
							float varP = var.cpixel(x + cx, y + cy);
							float varQ = var.cpixel(x + dx + cx, y + dy + cy);
							patchDist += (delta *delta - alpha * (varP + glm::min(varP, varQ))) / (1.0e-10 + k2 * (varP + varQ));
						}
					}

					patchDist /= 3.0;
					patchDist /= (2 * patchSize + 1) * (2 * patchSize + 1);

					float weight = exp(-glm::max(0.0f, patchDist));
					float pixel = image.cpixel(x + dx, y + dy);

					res += pixel * weight;
					weights += weight;
				}
			}

			result.pixel(x, y) = res / weights;
		}
	}
}

int main(int argc, char **argv) {
	srand(time(0));
	int width = 1333; int height = 768;

	try {
		Config *config = Config::getSingleton();
		if (argc >= 2) {
			config->readFile(argv[1]);
		} else {
			config->readFile("config.yaml");
		}

		width = config->var<int>("width") / 2;
		height = config->var<int>("height") / 2;

		Platform pl;
		pl.initTexture(width, height);

		Scene scene = AiDecoder().load(config->var<string>("scene").c_str());
		MyFormatEncoder().save(scene, "outputScene.txt");

		return 0;

		TImage<float> difference(width, height);
		TImage<float> differenceGauss(width, height);
		TImage<float> var(width, height);
		TImage<float> varDelta(width, height);
		TImage<float> varSigma(width, height);
		Image avgResult(width, height);
		Image result1(width, height);
		Image result2(width, height);

		Renderer *renderer = nullptr, *renderer2 = nullptr;
		if (config->var<string>("renderer") == "default") {
			renderer = new Renderer(width, height, *scene.camera, *scene.root);
			renderer2 = new Renderer(width, height, *scene.camera, *scene.root);
		} else {
			renderer = nullptr;// new FastRenderer(width, height, *scene.camera, *scene.root, scene.lights);
		}

		Sampler sampler(width, height);
		
		Uint32 timepassed = 0;
		bool rendering = true;
		pl.mainloop([&](Uint32 delta, Uint32 frametime) {
			if (rendering) {
				timepassed += delta;
				if (timepassed >= 1000 * 60) {
					timepassed = 0;
					pl.saveImage(renderer->imageData());
				}

				rendering = true;
				renderer->render(sampler);
				sampler.reset();
				renderer2->render(sampler);
				pl.updateTexture(0, renderer->image());
				pl.updateTexture(1, renderer2->image());

				variance(varDelta, renderer->image(), renderer2->image());
				variance(varSigma, renderer->varianceMap(), renderer2->varianceMap());
				nlMeans(var, varDelta, renderer->varianceMap(), varSigma);

				nlMeans(result1, renderer->image(), renderer2->image(), var);
				nlMeans(result2, renderer2->image(), renderer->image(), var);

				relativeDifference(difference, result1, result2);
				gauss(differenceGauss, difference);
				sampler.adjust(differenceGauss);
				pl.updateTexture(2, differenceGauss);

				average(avgResult, result1, result2);
				pl.updateTexture(3, avgResult);
				pl.updateTexture(4, var);
				pl.updateTexture(5, varSigma);
				pl.updateTexture(6, varDelta);

				if (!rendering) {
					pl.saveImage(renderer->imageData());
				}
			}
		});

		pl.saveImage(blend(renderer->imageData(), renderer2->imageData()));

	} catch (const Exception &e) {
		cout << e.toString() << endl;
		system("pause");
	}

	return 0;
}