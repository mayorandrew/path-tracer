#include "material/Lambert.h"
#include "tracer/Intersection.h"
#include "core/Common.h"

#include <glm.hpp>
#include <math.h>

using namespace glm;
using namespace core;

#define M_EPS 0.000006f

namespace material
{
	Lambert::Lambert(vec3 color) : m_color(color) {

	}

	bool Lambert::trace(tracer::Intersection &intersect, core::Ray *sample) const {
		vec3 tangent = intersect.tangent;
		vec3 bitangent = intersect.bitangent;
		float xi1 = std::uniform_real_distribution<float>(0, 1)(g_generator);
		float theta = acos(pow(1 - xi1, 1 / (1.0f + 1)));
		float alpha = std::uniform_real_distribution<float>(0, M_PI * 2)(g_generator);
		vec3 direction = (tangent * sin(alpha) + bitangent * cos(alpha)) * sin(theta) + intersect.normal * cos(theta);
		*sample = Ray(intersect.position + direction * 2.0f * M_EPS, direction);
		return true;
	}
	
	vec3 Lambert::blend(tracer::Intersection &intersect, core::Ray &incoming, vec3 value) const {
		return m_color * value; //using probability distribution
	}
}