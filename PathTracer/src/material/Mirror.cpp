#include "material/Mirror.h"
#include "tracer/Intersection.h"
#include "core/Common.h"

#include <glm.hpp>
#include <math.h>

using namespace glm;
using namespace core;

namespace material
{
	//todo try to optimize
	bool Mirror::trace(tracer::Intersection &intersect, core::Ray *sample) const {
		vec3 proj = dot(intersect.ray.dir(), intersect.normal) * intersect.normal;
		vec3 direction = intersect.ray.dir() - 2.0f * proj;

		*sample = Ray(intersect.position + direction * 0.05f, direction);
		return true;
	}
}