#include "material/Phong.h"
#include "tracer/Intersection.h"
#include "core/Common.h"

#include <glm.hpp>
#include <math.h>

using namespace glm;
using namespace core;

#define M_EPS 0.000006f

namespace material
{
	Phong::Phong(vec3 diffuse, vec3 specular, float specularity)
		: m_diffuse(diffuse), m_specular(specular), m_specularity(specularity) {

	}

	//todo try to optimize
	bool Phong::trace(tracer::Intersection &intersect, core::Ray *sample) const {
		vec3 tangent = core::anyPerpendicular(intersect.normal);
		vec3 bitangent = cross(intersect.normal, tangent);
		float theta = acos(std::uniform_real_distribution<float>(0, 1)(g_generator));
		float alpha = std::uniform_real_distribution<float>(0, M_PI * 2)(g_generator);
		vec3 direction = (tangent * sin(alpha) + bitangent * cos(alpha)) * sin(theta) + intersect.normal * cos(theta);
		*sample = Ray(intersect.position + direction * 2.0f * M_EPS, direction);
		return true;
	}

	vec3 Phong::blend(tracer::Intersection &intersect, core::Ray &incoming, vec3 value) const {
		vec3 reflected = -incoming.dir() + dot(incoming.dir(), intersect.normal) * intersect.normal * 2.0f;
		vec3 diffuse = m_diffuse * max(0.0f, dot(incoming.dir(), intersect.normal)) / float(M_PI);
		vec3 specular = m_specular * pow(max(0.0f, dot(reflected, -intersect.ray.dir())), m_specularity);
		return value * (diffuse + specular);
	}
}