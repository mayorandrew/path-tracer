#include "material/CookTorrance.h"
#include "tracer/Intersection.h"
#include "core/Common.h"
#include "core/Config.h"

#include <glm.hpp>
#include <math.h>

using namespace glm;
using namespace core;

#define M_EPS 0.000006f

namespace material
{
	CookTorrance::CookTorrance(
		Source *diffuse,
		Source *specular,
		glm::vec3 refraction,
		float roughness,
		float metallic,
		Source *normalmap)
		: m_diffuse(diffuse), m_specular(specular), m_ior(refraction), m_roughness(roughness), m_metallic(metallic), m_normalmap(normalmap) {
		m_twosided = Config::getSingleton()->var<bool>("twosided");
		m_numRaysGenerated = Config::getSingleton()->var<int>("numSecondaryRays");
	}

	CookTorrance::~CookTorrance() {
		if (m_diffuse) delete m_diffuse;
		if (m_specular) delete m_specular;
		if (m_normalmap) delete m_normalmap;
	}

	bool CookTorrance::trace(tracer::Intersection &intersect, std::vector<core::Ray> *sample, int depth) const {
		if (sample) {
			vec3 tangent = intersect.tangent;
			vec3 bitangent = intersect.bitangent;
			vec3 deltanormal = m_normalmap->getColor(intersect.texCoord.x, intersect.texCoord.y) * 2.0f - 1.0f;
			vec3 normal = transformNormal(deltanormal, tangent, intersect.normal, bitangent);

			float sign = dot(intersect.ray.dir(), normal);
			if (sign > 0) {
				if (m_twosided) {
					sign = -1;
					normal = -normal;
				} else {
					return false;
				}
			} else {
				sign = 1;
			}

			float xi1 = std::uniform_real_distribution<float>(0, 1)(g_generator);
			float theta = atan(m_roughness * sqrt(xi1) / sqrt(1 - xi1));
			float alpha = std::uniform_real_distribution<float>(0, M_PI * 2)(g_generator);

			vec3 halfvector = (tangent * sin(alpha) + bitangent * cos(alpha)) * sin(theta) + normal * cos(theta);
			vec3 direction = intersect.ray.dir() - 2 * dot(intersect.ray.dir(), halfvector) * halfvector;
			if (dot(direction, normal) <= 0) {
				sample->push_back(Ray(intersect.position, vec3(INFINITY)));
			} else {
				sample->push_back(Ray(intersect.position + sign * intersect.normal * 4.0f * M_EPS, direction));
			}
		}
		return true;
	}

	vec3 CookTorrance::blend(tracer::Intersection &intersect, const std::vector<core::Ray> &incomingA, const std::vector<glm::vec3> &valueA) const {
		vec3 tangent = intersect.tangent;
		vec3 bitangent = intersect.bitangent;
		vec3 deltanormal = m_normalmap->getColor(intersect.texCoord.x, intersect.texCoord.y) * 2.0f - 1.0f;
		vec3 normal = transformNormal(deltanormal, tangent, intersect.normal, bitangent);

		vec3 result(0.0f);

		for (int i = 0; i < incomingA.size(); i++) {
			if (length(valueA[i]) < M_EPS) {
				continue;
			}

			const Ray &incoming = incomingA[i];
			vec3 value = valueA[i];
			float cosNormalIn = dot(incoming.dir(), normal);
			float cosNormalOut = dot(normal, -intersect.ray.dir());

			if (cosNormalOut <= 0.0f) {
				if (m_twosided) {
					normal = -normal;
					cosNormalOut = -cosNormalOut;
					cosNormalIn = -cosNormalIn;
				} else {
					return vec3(0.0f);
				}
			}

			if (cosNormalIn <= 0.0f || cosNormalOut <= 0.0f) {
				return vec3(0.0f);
			}

			vec3 t_diffuse = m_diffuse->getColor(intersect.texCoord.x, intersect.texCoord.y);
			vec3 t_specular = m_specular->getColor(intersect.texCoord.x, intersect.texCoord.y);

			vec3 reflected = -incoming.dir() + cosNormalIn * normal * 2.0f;

			vec3 diffuse = t_diffuse;

			vec3 delta = incoming.dir() - intersect.ray.dir();
			if (length(delta) < 0.1f) {
				return vec3(0.0f);
			}

			vec3 halfvector = normalize(delta);
			float HdotN = dot(halfvector, normal);
			float VdotH = dot(-intersect.ray.dir(), halfvector);

			if (HdotN > 0.0f && VdotH > 0.0f) {
				float alpha2 = m_roughness * m_roughness;
				float HdotN2 = HdotN * HdotN;
				float den = HdotN2 * alpha2 + (1 - HdotN2);

				if (den < M_EPS) {
					return value;
				}

				float D = alpha2 / (M_PI * den * den);

				float VdotH2 = VdotH * VdotH;
				float tan2 = (1 - VdotH2) / VdotH2;
				float G = 2 / (1 + sqrt(1 + alpha2 * tan2));

				vec3 F0 = abs((vec3(1.0) - m_ior) / (vec3(1.0) + m_ior));
				vec3 fresnel0 = mix(F0 * F0, t_diffuse, m_metallic);
				vec3 F = fresnel0 + (vec3(1.0f) - fresnel0) * pow(1 - VdotH, 5);

				vec3 kspec = D * F * G / (4 * cosNormalOut * cosNormalIn + 0.05f);
				vec3 specular = t_specular * kspec;

				float probabilityS = (D * HdotN);

				result += cosNormalIn * value * (diffuse * (1 - m_metallic) + specular / probabilityS);
			} else {
				result += cosNormalIn * value * (diffuse);
			}
		}

		result /= incomingA.size();
		return result;
	}
}