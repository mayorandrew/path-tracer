#include "core/BBox.h"
#include "core/Ray.h"

using namespace glm;

namespace core
{
	BBox::BBox() : m_empty(true) {}
	BBox::BBox(glm::vec3 bottom_, glm::vec3 top_) : bottom(bottom_), top(top_), m_empty(false) {}

	void BBox::add(glm::vec3 point) {
		if (m_empty) {
			m_empty = false;
			bottom = top = point;
		} else {
			for (int i = 0; i < 3; i++) {
				if (point[i] < bottom[i]) bottom[i] = point[i];
				if (point[i] > top[i]) top[i] = point[i];
			}
		}
	}

	void BBox::add(const BBox &bbox) {
		if (m_empty) {
			m_empty = false;
			bottom = bbox.bottom; top = bbox.top;
		} else {
			for (int i = 0; i < 3; i++) {
				if (bbox.bottom[i] < bottom[i]) bottom[i] = bbox.bottom[i];
				if (bbox.top[i] > top[i]) top[i] = bbox.top[i];
			}
		}
	}

	BBox BBox::transform(glm::mat4 model) const {
		if (m_empty) return BBox();
		glm::vec3 points[8] = {
			glm::vec3(bottom.x, bottom.y, bottom.z),
			glm::vec3(bottom.x, bottom.y, top.z),
			glm::vec3(bottom.x, top.y, bottom.z),
			glm::vec3(bottom.x, top.y, top.z),
			glm::vec3(top.x, bottom.y, bottom.z),
			glm::vec3(top.x, bottom.y, top.z),
			glm::vec3(top.x, top.y, bottom.z),
			glm::vec3(top.x, top.y, top.z)
		};

		BBox res;

		for (int i = 0; i < 8; i++) {
			res.add(glm::vec3(model * glm::vec4(points[i], 1.0f)));
		}

		return res;
	}

	bool BBox::intersects(const core::Ray &ray) const {
		if (m_empty) return true;
		glm::vec3 invDir = glm::vec3(1.0f) / ray.dir();

		float lo = invDir.x * (bottom.x - ray.pos().x);
		float hi = invDir.x * (top.x - ray.pos().x);
		float tmin = min(lo, hi);
		float tmax = max(lo, hi);

		float lo1 = invDir.y * (bottom.y - ray.pos().y);
		float hi1 = invDir.y * (top.y - ray.pos().y);
		tmin = max(tmin, min(lo1, hi1));
		tmax = min(tmax, max(lo1, hi1));

		float lo2 = invDir.z * (bottom.z - ray.pos().z);
		float hi2 = invDir.z * (top.z - ray.pos().z);
		tmin = max(tmin, min(lo2, hi2));
		tmax = min(tmax, max(lo2, hi2));

		return (tmin <= tmax) && (tmax > 0.f);
	}
}