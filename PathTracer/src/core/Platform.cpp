#include "core\Platform.h"
#include "core\Exception.h"
#include "core\Config.h"

#include <SDL_image.h>

#include "FreeImage.h"

#include <iostream>

using namespace std;

namespace core
{

	Platform::Platform() : m_window(nullptr), m_renderer(nullptr), m_numTextures(9) {
		if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
			throw Exception("SDL_Init Error: " + string(SDL_GetError()));
		}
		int flags = IMG_INIT_PNG | IMG_INIT_JPG;
		if ((IMG_Init(flags) & flags) == 0) {
			throw Exception("IMG_Init Error: " + string(IMG_GetError()));
		}
		try {

			m_width = Config::getSingleton()->var<int>("width");
			m_height = Config::getSingleton()->var<int>("height");

			if (!(m_window = SDL_CreateWindow("PathTracer", 100, 100, m_width, m_height, SDL_WINDOW_SHOWN)))
				throw Exception("SDL_CreateWindow Error: " + string(SDL_GetError()));

			if (!(m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)))
				throw Exception("SDL_CreateRenderer Error: " + string(SDL_GetError()));

		} catch (Exception &e) {
			if (m_renderer) SDL_DestroyRenderer(m_renderer);
			if (m_window) SDL_DestroyWindow(m_window);
			SDL_Quit();
			throw e;
		}

		m_texture = new SDL_Texture*[m_numTextures];
		for (int i = 0; i < m_numTextures; i++) {
			m_texture[i] = nullptr;
		}

		FreeImage_Initialise();
	}

	Platform::~Platform() {
		FreeImage_DeInitialise();
		IMG_Quit();
		for (int i = 0; i < m_numTextures; i++)
			SDL_DestroyTexture(m_texture[i]);
		delete[] m_texture;
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
	}

	void Platform::initTexture(int width, int height) {
		for (int i = 0; i < m_numTextures; i++)
			if (!(m_texture[i] = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, width, height)))
				throw Exception("SDL_CreateTextureFromSurface Error: " + string(SDL_GetError()));
	}

	void Platform::updateTexture(int i, const Image &img) {
		SDL_UpdateTexture(m_texture[i], NULL, img.data(), img.width() * sizeof(*img.data()));
	}

	void Platform::updateTexture(int i, const TImage<float> &img) {
		Image im(img.width(), img.height());
		float max = img.max();
		for (int j = 0; j < im.width() * im.height(); j++) {
			int val = img[j] * 255 / max;
			im[j] = val + (val << 8) + (val << 16);
		}
		SDL_UpdateTexture(m_texture[i], NULL, im.data(), im.width() * sizeof(*im.data()));
	}

	void Platform::render() {
		SDL_RenderClear(m_renderer);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				SDL_Rect rect;
				rect.x = m_width / 3 * j;
				rect.y = m_height / 3 * i;
				rect.w = m_width / 3;
				rect.h = m_height / 3;
				SDL_RenderCopy(m_renderer, m_texture[i * 3 + j], NULL, &rect);
			}
		}
		SDL_RenderPresent(m_renderer);
		SDL_Delay(1);
	}

	void Platform::mainloop(std::function<void(Uint32, Uint32)> frame) {
		SDL_Event event;
		Uint32 frametime;
		Uint32 minframetime = 1000 / 10;
		Uint32 lastframe = minframetime;
		bool running = true;
		while (running) {
			frametime = SDL_GetTicks();

			while (SDL_PollEvent(&event) != 0) {
				switch (event.type) {
				case SDL_KEYDOWN: if (event.key.keysym.sym == SDLK_ESCAPE)
					running = false;
					break;
				case SDL_QUIT: running = false; break;
				}
			}

			frame(lastframe, minframetime);
			render();

			lastframe = SDL_GetTicks() - frametime;
			if (lastframe < minframetime)
				SDL_Delay(minframetime - lastframe);
		}
	}

	void Platform::saveImage(const TImage<glm::vec4> &image) const {
		FIBITMAP *bitmap = FreeImage_AllocateT(FIT_RGB16, image.width(), image.height());

		Config *config = Config::getSingleton();

		vector<float> r, g, b;
		unsigned pitch = FreeImage_GetPitch(bitmap);
		BYTE *bits = (BYTE*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < image.height(); y++) {
			FIRGB16 *pixel = (FIRGB16*)bits;
			for (int x = 0; x < image.width(); x++) {
				if (config->var<bool>("medianFilter")) {
					r.clear(); g.clear(); b.clear();
					for (int y1 = max(y - 1, 0); y1 <= min(y + 1, image.height() - 1); y1++) {
						for (int x1 = max(x - 1, 0); x1 <= min(x + 1, image.width() - 1); x1++) {
							glm::vec4 pix = image.pixel(x1, image.height() - 1 - y1);
							r.push_back(pix.r);
							g.push_back(pix.g);
							b.push_back(pix.b);
						}
					}

					sort(r.begin(), r.end());
					sort(g.begin(), g.end());
					sort(b.begin(), b.end());
					int middle = r.size() / 2;
					glm::vec3 pix(r[middle], g[middle], b[middle]);
					pixel[x].red = pix.r * 0xFFFF;
					pixel[x].green = pix.g * 0xFFFF;
					pixel[x].blue = pix.b * 0xFFFF;
				} else {
					glm::vec4 pix = image.pixel(x, image.height() - 1 - y);
					pixel[x].red = pix.r * 0xFFFF;
					pixel[x].green = pix.g * 0xFFFF;
					pixel[x].blue = pix.b * 0xFFFF;
				}
			}
			bits += pitch;
		}

		FreeImage_Save(FIF_PNG, bitmap, Config::getSingleton()->var<string>("output").c_str());
		FreeImage_Unload(bitmap);
	}

	Uint32 Platform::getPixel(SDL_Surface *surface, int x, int y) {
		int bpp = surface->format->BytesPerPixel;
		Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

		switch (bpp) {
		case 1:
			return *p;
			break;

		case 2:
			return *(Uint16 *)p;
			break;

		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
			break;

		case 4:
			return *(Uint32 *)p;
			break;

		default:
			return 0;
		}
	}

	TImage<glm::vec3> Platform::loadTexture(const std::string &path) {
		SDL_Surface *surface = IMG_Load(path.c_str());
		TImage<glm::vec3> img(surface->w, surface->h);

		for (int y = 0; y < surface->h; y++) {
			for (int x = 0; x < surface->w; x++) {
				Uint32 pixel = Platform::getPixel(surface, x, y);
				Uint8 r, g, b, a;
				SDL_GetRGBA(pixel, surface->format, &r, &g, &b, &a);
				img.pixel(x, y) = glm::vec3(r / 255.0f, g / 255.0f, b / 255.0f);
			}
		}

		SDL_FreeSurface(surface);
		return std::move(img);
	}

}