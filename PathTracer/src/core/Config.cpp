#include "core/Config.h"

using namespace std;

namespace core
{
	Config::Config() {

	}

	Config::~Config() {

	}

	Config *Config::m_config = nullptr;

	Config *Config::getSingleton() {
		if (!m_config) m_config = new Config();
		return m_config;
	}

	void Config::readFile(const string &filename) {
		m_data = YAML::LoadFile(filename.c_str());
	}
}