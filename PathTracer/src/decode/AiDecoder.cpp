#include "decode/AiDecoder.h"
#include "tracer/ShapeSceneNode.h"
#include "shape/Mesh.h"
#include "shape/Sphere.h"

#include "material/CookTorrance.h"
#include "material/Lambert.h"
#include "material/Mirror.h"
#include "material/Light.h"
#include "material/Phong.h"

#include "core/Platform.h"
#include "core/Exception.h"
#include "core/Config.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <fstream>
#include <iostream>

using namespace material;
using namespace tracer;
using namespace shape;
using namespace core;
using namespace std;
using namespace glm;

namespace decode
{
	TImage<glm::vec3> AiDecoder::_convertHeightmapToNormalmap(const TImage<glm::vec3> &images, float scale) {
		int nHeight = images.height(); int nWidth = images.width();
		TImage<glm::vec3> normalmap(nWidth, nHeight);

		for (int y = 0; y < nHeight; ++y) {
			for (int x = 0; x < nWidth; ++x) {
				vec3 p = normalize(vec3(
					images.pixel(x > 0 ? x - 1 : nWidth - 1, y).r - images.pixel(x < nWidth - 1 ? x + 1 : 0, y).r,
					2.0f / scale,
					images.pixel(x, y > 0 ? y - 1 : nHeight - 1).r - images.pixel(x, y < nHeight - 1 ? y + 1 : 0).r
				));
				p = (p + vec3(1.0f)) / 2.0f;

				normalmap[y * nWidth + x] = p;
			}
		}

		return std::move(normalmap);
	}

	void AiDecoder::_convertAiNodeToSceneNode(aiNode *sNode, SceneNode *tNode) {
		tNode->name = sNode->mName.C_Str();

		m_registry[tNode->name] = tNode;

		mat4 mat;
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; k++) {
				mat[k][j] = sNode->mTransformation[j][k];
			}
		}
		tNode->setRelMatrix(mat);

		for (int i = 0; i < sNode->mNumMeshes; i++) {
			int shapeId = sNode->mMeshes[i];
			int matId = aiscene->mMeshes[shapeId]->mMaterialIndex;
			ShapeSceneNode *nNode = new ShapeSceneNode(*m_scene->shapes[shapeId], *m_scene->materials[matId], tNode);
		}

		for (int i = 0; i < sNode->mNumChildren; i++) {
			SceneNode *nNode = new SceneNode(tNode);
			_convertAiNodeToSceneNode(sNode->mChildren[i], nNode);
		}
	}

	tracer::Scene AiDecoder::load(const std::string &filepath) {
		Assimp::Importer importer;
		unsigned flags = aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_CalcTangentSpace;
		if (Config::getSingleton()->var<bool>("splitLargeMeshes")) {
			flags = flags | aiProcess_SplitLargeMeshes;
			importer.SetPropertyInteger(AI_CONFIG_PP_SLM_VERTEX_LIMIT, Config::getSingleton()->var<int>("splitVertexLimit"));
			importer.SetPropertyInteger(AI_CONFIG_PP_SLM_TRIANGLE_LIMIT, Config::getSingleton()->var<int>("splitTriangleLimit"));
		}
		importer.ReadFile(filepath, flags);
		aiscene = importer.GetOrphanedScene();

		Scene scene;
		m_scene = &scene;

		for (int i = 0; i < aiscene->mNumMeshes; i++) {
			const aiMesh *sMesh = aiscene->mMeshes[i];
			Mesh *tMesh = new Mesh();
			if (sMesh->HasPositions()) {
				for (int j = 0; j < sMesh->mNumVertices; j++) {
					const aiVector3D &pos = sMesh->mVertices[j];
					tMesh->vertices.push_back(vec3(pos.x, pos.y, pos.z));
				}
			} else {
				throw core::Exception("Positions are needed");
			}
			if (sMesh->HasNormals()) {
				for (int j = 0; j < sMesh->mNumVertices; j++) {
					const aiVector3D &nor = sMesh->mNormals[j];
					vec3 norm = vec3(nor.x, nor.y, nor.z);
					tMesh->normals.push_back(normalize(norm));
				}
			} else {
				throw core::Exception("Normals are needed");
			}
			if (sMesh->HasTextureCoords(0)) {
				for (int j = 0; j < sMesh->mNumVertices; j++) {
					const aiVector3D &pos = sMesh->mTextureCoords[0][j];
					tMesh->texCoords.push_back(vec2(pos.x, pos.y));
				}
			} else {
				throw core::Exception("Texture coords are needed");
			}
			if (sMesh->HasTangentsAndBitangents()) {
				for (int j = 0; j < sMesh->mNumVertices; j++) {
					const aiVector3D &tan = sMesh->mTangents[j];
					vec3 tann = vec3(tan.x, tan.y, tan.z);
					auto res = isnan(tann);
					if (res.x || res.y || res.z) {
						//do something
						tMesh->tangents.push_back(anyPerpendicular(tMesh->normals[j]));
					} else {
						tMesh->tangents.push_back(normalize(tann));
					}
				}
				for (int j = 0; j < sMesh->mNumVertices; j++) {
					const aiVector3D &tan = sMesh->mBitangents[j];
					vec3 tann = vec3(tan.x, tan.y, tan.z);
					auto res = isnan(tann);
					if (res.x || res.y || res.z) {
						//do something
						tMesh->bitangents.push_back(cross(tMesh->normals[j], tMesh->tangents[j]));
					} else {
						tMesh->bitangents.push_back(normalize(tann));
					}
				}
			} else {
				throw core::Exception("Tangents and bitangents are needed.");
			}
			if (sMesh->HasFaces()) {
				SubMesh submesh;
				for (int j = 0; j < sMesh->mNumFaces; j++) {
					const aiFace &face = sMesh->mFaces[j];
					if (face.mNumIndices != 3) {
						continue;
					}
					Triangle trig;
					for (int k = 0; k < 3; k++) {
						trig.vertices[k] = trig.texCoords[k] = trig.normals[k] = trig.tangents[k] = trig.bitangents[k] = face.mIndices[k];
					}
					submesh.triangles.push_back(trig);
				}
				if (submesh.triangles.size() > 0) {
					tMesh->submeshes.push_back(submesh);
				}
			}
			if (tMesh->submeshes.size() > 0) {
				tMesh->prepare();
				scene.shapes.push_back(tMesh);
			} else {
				delete tMesh;
			}
		}

		for (int i = 0; i < aiscene->mNumMaterials; i++) {
			aiMaterial *mat = aiscene->mMaterials[i];

			aiColor3D diffuse;
			mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);

			aiColor3D emission;
			mat->Get(AI_MATKEY_COLOR_EMISSIVE, emission);

			aiColor3D specular;
			mat->Get(AI_MATKEY_COLOR_SPECULAR, specular);

			float reflectivity;
			mat->Get(AI_MATKEY_REFLECTIVITY, reflectivity);

			float shininess;
			mat->Get(AI_MATKEY_SHININESS, shininess);

			if (!emission.IsBlack() || mat->GetTextureCount(aiTextureType_EMISSIVE) > 0) {
				Source *emissionmap = nullptr;

				if (mat->GetTextureCount(aiTextureType_EMISSIVE) > 0) {
					aiString path; mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
					TImage<glm::vec3> diffuse = Platform::loadTexture(path.C_Str());
					emissionmap = new TextureSource(std::move(diffuse));
				} else {
					emissionmap = new ColorSource(vec3(emission.r, emission.g, emission.b));
				}
				scene.materials.push_back(new Light(emissionmap));
			} else {

				Source *diffusemap = nullptr;
				Source *normalmap = nullptr;

				if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
					aiString path; mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
					TImage<glm::vec3> diffuse = Platform::loadTexture(path.C_Str());
					diffusemap = new TextureSource(std::move(diffuse));
				} else {
					diffusemap = new ColorSource(vec3(diffuse.r, diffuse.g, diffuse.b));
				}

				if (mat->GetTextureCount(aiTextureType_NORMALS) > 0) {
					aiString path; mat->GetTexture(aiTextureType_NORMALS, 0, &path);
					TImage<glm::vec3> diffuse = Platform::loadTexture(path.C_Str());
					TImage<glm::vec3> normals = _convertHeightmapToNormalmap(diffuse, 10.0f);
					normalmap = new TextureSource(std::move(normals));
				} else {
					normalmap = new ColorSource(vec3(0.5, 1, 0.5));
				}

				scene.materials.push_back(new CookTorrance(
					diffusemap,
					new ColorSource(vec3(specular.r, specular.g, specular.b)),
					vec3(1, 1, 1),
					Config::getSingleton()->var<float>("baseShininess") / shininess,
					1.0f - reflectivity,
					normalmap
					));

			}
		}

		aiNode *root = aiscene->mRootNode;
		_convertAiNodeToSceneNode(root, scene.root);

		scene.root->evaluate();

		for (int i = 0; i < aiscene->mNumLights; i++) {
			aiLight *light = aiscene->mLights[i];

			mat4 transform = m_registry[string(light->mName.C_Str())]->getMatrix();

			vec3 position = vec3(transform * vec4(light->mPosition.x, light->mPosition.y, light->mPosition.z, 1.0f));
			vec3 color = vec3(light->mColorDiffuse.r, light->mColorDiffuse.g, light->mColorDiffuse.b);
			scene.lights.push_back(PointLight(position, color));
		}

		if (aiscene->HasCameras()) {
			aiCamera *cam = aiscene->mCameras[0];
			aiVector3D dir = cam->mLookAt;

			mat4 transform = m_registry[string(cam->mName.C_Str())]->getMatrix();
			vec3 up = vec3(cam->mUp.x, cam->mUp.y, cam->mUp.z);
			vec3 direction = vec3(dir.x, dir.y, dir.z);
			vec3 position = vec3(cam->mPosition.x, cam->mPosition.y, cam->mPosition.z);

			up = normalize(vec3(transform * vec4(up, 0.0f)));
			direction = normalize(vec3(transform * vec4(direction, 0.0f)));
			position = vec3(transform * vec4(position, 1.0f));

			scene.camera = new Camera(Config::getSingleton()->var<int>("width"), Config::getSingleton()->var<int>("height"), cam->mHorizontalFOV / 2.0f, up, direction, position);
		} else {
			Config *config = Config::getSingleton();
			scene.camera = new Camera(config->var<int>("width"), config->var<int>("height"), config->var<int>("defaultFov") * M_PI / 180.0f);
			scene.camera->setPosition(config->var<vec3>("defaultCameraPosition"));
			scene.camera->setDirection(config->var<vec3>("defaultCameraDirection"));
		}

		//delete aiscene; <-- Hangs program for complex scenes

		return std::move(scene);
	}
}