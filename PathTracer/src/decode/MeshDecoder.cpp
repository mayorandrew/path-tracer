#include "decode/MeshDecoder.h"

#include <fstream>
#include <sstream>

using namespace shape;
using namespace std;
using namespace glm;

namespace decode
{
	Mesh MeshDecoder::load(const string &filename) {
		ifstream input(filename);
		string line;

		Mesh mesh;
		SubMesh tsubmesh;

		while (getline(input, line)) {
			if (line.size() == 0) continue;
			if (line[0] == '#') continue;
			istringstream in(line);
			string word;
			in >> word;
			if (word == "v") {
				vec3 pos;
				for (int i = 0; i < 3; i++) {
					in >> pos[i];
				}
				mesh.vertices.push_back(pos);
			} else if (word == "vt") {
				vec2 tex;
				for (int i = 0; i < 2; i++) {
					in >> tex[i];
				}
				mesh.texCoords.push_back(tex);
			} else if (word == "vn") {
				vec3 norm;
				for (int i = 0; i < 3; i++) {
					in >> norm[i];
				}
				mesh.normals.push_back(norm);
			} else if (word == "f") {
				Triangle trig;
				for (int i = 0; i < 3; i++) {
					char slash;
					in >> trig.vertices[i] >> slash >> trig.texCoords[i] >> slash >> trig.normals[i];
					trig.vertices[i]--; trig.texCoords[i]--; trig.normals[i]--;
				}
				tsubmesh.triangles.push_back(trig);
			} else if (word == "g") {
				mesh.submeshes.push_back(tsubmesh);
				tsubmesh = SubMesh();
			}
		}

		mesh.submeshes.push_back(tsubmesh);

		mesh.prepare();

		return mesh;
	}
}