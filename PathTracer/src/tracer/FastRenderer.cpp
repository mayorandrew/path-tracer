#include "tracer\FastRenderer.h"

#include "core\Ray.h"
#include "core\Common.h"
#include "core\Config.h"

#include <iostream>
#include <gtx/norm.hpp>

using namespace core;
using namespace glm;
using namespace std;

namespace tracer
{
	FastRenderer::FastRenderer(int targetWidth, int targetHeight, Camera &cam, SceneNode &scene, std::vector<tracer::PointLight> &lights) :
		AbstractRenderer(targetWidth, targetHeight), m_camera(cam),
		m_scene(scene), m_renderStep(0), m_pixelsPerStep(1000), m_maxRayDepth(10),
		m_adjust(true), m_lights(lights) {
		m_camera.setDimensions(targetWidth, targetHeight);

		Config *config = Config::getSingleton();
		m_pixelsPerStep = config->var<int>("pixelsPerStep");
		m_maxRayDepth = config->var<int>("maxRayDepth");
		m_adjust = config->var<bool>("adjustFramerate");
	}

	FastRenderer::~FastRenderer() {
	}

	void FastRenderer::adjust(Uint32 delta, Uint32 frametime) {
		if (m_adjust) {
			m_pixelsPerStep = glm::max(int(m_pixelsPerStep * (float(frametime) / delta)), 1000);
		}
	}

	vec3 FastRenderer::traceRay(const Ray &r, int depth) const {
		if (depth == m_maxRayDepth) {
			return vec3(0.0f);
		}

		Intersection intersect;
		if (m_scene.intersects(r, &intersect)) {
			if (intersect.material->trace(intersect)) {
				vector<Ray> newRays;
				vector<vec3> values;

				for (int i = 0; i < m_lights.size(); i++) {
					vec3 lightDir = normalize(m_lights[i].position - intersect.position);
					newRays.push_back(Ray(intersect.position + lightDir * 0.01f, lightDir));
					values.push_back(m_lights[i].color);
				}

				return intersect.material->blend(intersect, newRays, values);
			} else {
				return intersect.material->intensity(intersect);
			}
		} else {
			return vec3(0.0f);
		}
	}

	bool FastRenderer::render() {
		int imWidth = m_image.width();
		int imHeight = m_image.height();
		int startPixel = m_renderStep;
		int endPixel = glm::min(m_renderStep + m_pixelsPerStep, imWidth * imHeight);

#pragma omp parallel for
		for (int p = startPixel; p < endPixel; p++) {
			int y = static_cast<int>(p / imWidth);
			int x = (p % imWidth);

			Ray r = m_camera.ray(float(x), float(y));

			vec3 value = traceRay(r);
			m_sampleData.pixel(x, y) = vec4(value, 1);
			value = gammaCorrect(value);
			m_image.pixel(x, y) = rgb(value.x, value.y, value.z);
		}

		m_renderStep += m_pixelsPerStep;

		if (endPixel == imWidth * imHeight)
			return false;
		return true;
	}
}
