#include "tracer\Renderer.h"

#include "core\Ray.h"
#include "core\Common.h"
#include "core\Config.h"

#include <iostream>
#include <gtx/norm.hpp>

using namespace core;
using namespace glm;
using namespace std;

namespace tracer
{
	Renderer::Renderer(int targetWidth, int targetHeight, Camera &cam, SceneNode &scene) :
		AbstractRenderer(targetWidth, targetHeight), m_camera(cam), 
		m_scene(scene), m_renderStep(0), m_pixelsPerStep(1000), m_maxRayDepth(10), m_pixelSize(0),
		m_adjust(true), m_varMap(targetWidth, targetHeight) {
		m_camera.setDimensions(targetWidth, targetHeight);

		Config *config = Config::getSingleton();
		m_pixelsPerStep = config->var<int>("pixelsPerStep");
		m_maxRayDepth = config->var<int>("maxRayDepth");
		m_pixelSize = config->var<int>("pixelSize");
		m_adjust = config->var<bool>("adjustFramerate");
	}

	Renderer::~Renderer() {
	}

	void Renderer::adjust(Uint32 delta, Uint32 frametime) {
		if (m_adjust) {
			m_pixelsPerStep = glm::max(int(m_pixelsPerStep * (float(frametime) / delta)), 1000);
		}
	}

	vec3 Renderer::traceRay(const Ray &r, int depth) const {
		if (depth == m_maxRayDepth) {
			return vec3(0.0f);
		}

		Intersection intersect;
		if (m_scene.intersects(r, &intersect)) {
			vector<Ray> newRays;
			if (intersect.material->trace(intersect, &newRays, depth)) {
				vector<vec3> values;
				for (const Ray &r : newRays) {
					if (isnan(r.dir()).x) {
						values.push_back(vec3(0.0f));
					} else {
						values.push_back(traceRay(r, depth + 1));
					}
				}
				return intersect.material->blend(intersect, newRays, values);
			} else {
				return intersect.material->intensity(intersect);
			}
		} else {
			return vec3(0.0f);
		}
	}

	bool Renderer::render(Sampler &sampler) {
		int imWidth = m_image.width();
		int imHeight = m_image.height();

		vector<ivec2> points = sampler.sampleArray();

#pragma omp parallel for
		for (int i = 0; i < points.size(); i++) {
			ivec2 point = points[i];
			float x = point.x;
			float y = point.y;

			Ray r = m_camera.ray(x, y);
			vec3 value = traceRay(r);

			for (int dx = -m_pixelSize; dx <= m_pixelSize; dx++) {
				for (int dy = -m_pixelSize; dy <= m_pixelSize; dy++) {
					int cx = x + dx;
					int cy = y + dy;
					if (cx < 0 || cy < 0 || cx >= imWidth || cy >= imHeight) continue;

					vec4 data = m_sampleData.pixel(cx, cy);
					float n = data.w;
					if (n == 0) {
						m_sampleData.pixel(cx, cy) = vec4(value, 1);
						value = gammaCorrect(value);
						m_image.pixel(cx, cy) = rgb(value.x, value.y, value.z);
						m_varMap.pixel(cx, cy) = 0;
					} else {
						n++;
						vec3 nvalue = (vec3(data) * (n - 1) + value) / n;
						m_sampleData.pixel(cx, cy) = vec4(nvalue, n);
						nvalue = gammaCorrect(nvalue);
						m_image.pixel(cx, cy) = rgb(nvalue.x, nvalue.y, nvalue.z);
						float nvar = (n - 2) * m_varMap.pixel(cx, cy) / (n - 1) + length2(vec3(data) - value) / (n + 1);
						m_varMap.pixel(cx, cy) = nvar;
					}
				}
			}
		}
		return true;
	}

	bool Renderer::renderRays() {
		int imWidth = m_image.width();
		int imHeight = m_image.height();
		int startPixel = m_renderStep;
		int endPixel = glm::min(m_renderStep + m_pixelsPerStep, imWidth * imHeight);

		for (int p = startPixel; p < endPixel; p++) {
			int y = static_cast<int>(p / imWidth);
			int x = (p % imWidth);

			Ray r = m_camera.ray(float(x), float(y));

			vec3 value = traceRay(r);
			m_sampleData.pixel(x, y) = vec4(value, 1);
			value = gammaCorrect(value);
			m_image.pixel(x, y) = rgb(value.x, value.y, value.z);
		}

		m_renderStep += m_pixelsPerStep;

		if (endPixel == imWidth * imHeight)
			return false;
		return true;
	}
}
