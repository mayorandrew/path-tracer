#include "tracer\Camera.h"

#include <math.h>

using namespace glm;
using namespace core;

namespace tracer
{
	Camera::Camera(int width, int height, float angle, vec3 up, vec3 dir, vec3 pos) :
		m_position(pos), m_direction(dir), m_width(width), m_height(height), m_up(up), m_angleH(angle) {
		m_angleV = atan(tan(m_angleH) * m_height / m_width);
		_update();
	}

	Camera::~Camera() {
	}

	void Camera::_update() {
		m_left = normalize(cross(m_up, m_direction)) * tan(m_angleH);
		m_top = normalize(cross(m_direction, m_left)) * tan(m_angleV);
		m_corner = m_direction + m_left + m_top;
		m_stepX = 2.0f / m_width;
		m_stepY = 2.0f / m_height;
	}

	Ray Camera::ray(float x, float y) const {
		return Ray(m_position, m_corner - m_left * x * m_stepX - m_top * y * m_stepY);
	}
}
