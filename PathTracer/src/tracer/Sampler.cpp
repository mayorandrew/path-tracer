#include "../../include/tracer/Sampler.h"

#include "core\Common.h"

#include <iostream>

using namespace core;
using namespace glm;
using namespace std;

namespace tracer
{
	Sampler::Sampler(int width, int height) : m_map(width, height), m_xDistrib(0, width), m_yDistrib(0, height) {
		m_adjusted = false;
		reset();
	}

	void Sampler::adjust(const TImage<float> &image) {
		m_adjusted = true;
		m_map = image;
		m_map += 0.1f;
		m_map *= num() / m_map.sum();
		reset();
	}

	ivec2 Sampler::sample() {
		if (!m_adjusted) {
			m_cx++;
			if (m_cx >= num()) {
				return ivec2(-1, -1);
			}
			return ivec2(m_xDistrib(g_generator), m_yDistrib(g_generator));
		}

		float m_maxsamples = m_map.pixel(m_cx, m_cy);
		while (m_csample >= m_maxsamples) {
			m_csample = 0;
			m_cx++;
			if (m_cx >= m_map.width()) {
				m_cx = 0;
				m_cy++;
				if (m_cy >= m_map.height()) {
					return ivec2(-1, -1);
				}
			}
			m_maxsamples = m_map.pixel(m_cx, m_cy);
		}

		if (m_csample + 1 <= m_maxsamples) {
			m_csample++;
			return ivec2(m_cx, m_cy);
		} else {
			m_csample++;
			// todo probability
			return ivec2(m_cx, m_cy);
		}
	}

	vector<ivec2> Sampler::sampleArray() {
		unsigned long long time = __rdtsc();
		vector<ivec2> pixels;

		ivec2 pixel = sample();
		while (pixel != ivec2(-1, -1)) {
			pixels.push_back(pixel);
			pixel = sample();
		}

		unsigned long long delta = __rdtsc() - time;
		cout << delta << endl;
		return pixels;
	}

}