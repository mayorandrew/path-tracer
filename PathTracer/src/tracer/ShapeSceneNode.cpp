#include "tracer/ShapeSceneNode.h"

using namespace glm;

namespace tracer
{
	ShapeSceneNode::ShapeSceneNode(const shape::Shape &shape, material::Material &material, SceneNode *parent) :
		SceneNode(parent), m_shape(shape), m_material(material) {
	}
	
	ShapeSceneNode::~ShapeSceneNode() {}

	void ShapeSceneNode::evaluateMatrix() {
		SceneNode::evaluateMatrix();
		m_bbox = m_shape.getBBox().transform(m_modelmatrix);
	}

	bool ShapeSceneNode::intersects(const core::Ray &ray, Intersection *intersect) {
		bool res = SceneNode::intersects(ray, intersect);

		core::Ray nRay = ray.transform(m_invmodelmatrix);
		if (intersect) {
			Intersection nint;
			if (m_shape.intersects(nRay, &nint)) {
				nint.transform(m_modelmatrix, m_normalmatrix);
				if (!res || nint.distance < intersect->distance) {
					*intersect = nint;
					intersect->material = &m_material;
				}
				res = true;
			}
		} else {
			res = res || m_shape.intersects(nRay);
		}
		return res;
	};
}