#include "tracer/SceneNode.h"

#include "core/BBox.h"

using namespace core;

namespace tracer
{
	SceneNode::SceneNode(SceneNode *parent) : m_parent(parent) {
		if (parent) parent->add(this);
	}
	SceneNode::~SceneNode() {
		for (unsigned i = 0; i < m_children.size(); i++) { delete m_children[i]; }
	}

	bool SceneNode::intersects(const core::Ray &ray, Intersection *intersect) {
		bool res = false;
		if (intersect) {
			for (unsigned i = 0; i < m_children.size(); i++) {
				const BBox &bbox = m_children[i]->getBBox();
				if (bbox.intersects(ray)) {
					Intersection nint;
					if (m_children[i]->intersects(ray, &nint)) {
						if (!res || nint.distance < intersect->distance) {
							*intersect = nint;
							intersect->node = m_children[i];
						}
						res = true;
					}
				}
			}
		} else {
			for (unsigned i = 0; i < m_children.size(); i++) {
				const BBox &bbox = m_children[i]->getBBox();
				if (bbox.intersects(ray)) {
					if (m_children[i]->intersects(ray)) return true;
				}
			}
		}
		return res;
	};

	void SceneNode::evaluateMatrix() {
		if (m_parent) {
			setMatrix(m_parent->getMatrix() * m_relmodelmatrix);
		}
	}

	void SceneNode::evaluate() {
		evaluateMatrix();
		for (unsigned i = 0; i < m_children.size(); i++) {
			m_children[i]->evaluate();
			m_bbox.add(m_children[i]->getBBox());
		}
	}

	void SceneNode::add(SceneNode *child) {
		m_children.push_back(child);
	}

	void SceneNode::setMatrix(const glm::mat4 &mat) {
		m_modelmatrix = mat;
		m_invmodelmatrix = glm::inverse(m_modelmatrix);
		m_normalmatrix = glm::inverseTranspose(glm::mat3(m_modelmatrix));
	}

	void SceneNode::setRelMatrix(const glm::mat4 &mat) {
		m_relmodelmatrix = mat;
	}
}