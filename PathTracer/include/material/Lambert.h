#pragma once

#include "Material.h"

namespace material
{
	class Lambert : public Material
	{
	public:
		//albedo should be >0 and <=1
		Lambert(glm::vec3 color = glm::vec3(1, 1, 1));

		virtual bool trace(tracer::Intersection &intersect, core::Ray *sample = nullptr) const;
		virtual glm::vec3 blend(tracer::Intersection &intersect, core::Ray &incoming, glm::vec3 value) const;

	private:
		glm::vec3 m_color;
	};
}