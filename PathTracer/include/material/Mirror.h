#pragma once
#include "material\Material.h"

namespace material
{
	class Mirror :
		public Material
	{
	public:
		virtual bool trace(tracer::Intersection &intersect, core::Ray *sample = nullptr) const;
	};
}
