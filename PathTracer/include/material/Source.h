#pragma once

#include "core/Image.h"
#include <glm.hpp>

namespace material
{
	class Source
	{
	public:
		virtual glm::vec3 getColor(float u, float v) const { return glm::vec3(); }
	};

	class ColorSource : public Source
	{
	public:
		ColorSource(glm::vec3 color) : m_color(color) {};
		virtual glm::vec3 getColor(float u, float v) const { return m_color; }

	protected:
		glm::vec3 m_color;
	};

	class TextureSource : public Source
	{
	public:
		TextureSource(core::TImage<glm::vec3> &&texture) : m_image(texture) {};
		virtual glm::vec3 getColor(float u, float v) const {
			u = u - floor(u); v = v - floor(v);
			int x = u * m_image.width(); int y = m_image.height() - 1 - v * m_image.height();
			return m_image.pixel(x, y);
		}

	protected:
		core::TImage<glm::vec3> m_image;
	};
}