#pragma once

#include "Material.h"
#include "Source.h"

namespace material
{
	class CookTorrance : public Material
	{
	public:
		CookTorrance(
			Source *diffuse = new ColorSource(glm::vec3(1, 1, 1)),
			Source *specular = new ColorSource(glm::vec3(0, 0, 0)),
			glm::vec3 refraction = glm::vec3(1, 1, 1),
			float roughness = 0.8f,
			float metallic = 0.0f,
			Source *normalmap = new ColorSource(glm::vec3(0.5f, 1.0f, 0.5f))
		);

		~CookTorrance();

		virtual bool trace(tracer::Intersection &intersect, std::vector<core::Ray> *sample = nullptr, int depth = 0) const;
		virtual glm::vec3 blend(tracer::Intersection &intersect, const std::vector<core::Ray> &incoming, const std::vector<glm::vec3> &value) const;

	private:
		Source *m_diffuse;
		Source *m_specular;
		Source *m_normalmap;
		glm::vec3 m_ior;
		float m_roughness;
		float m_metallic;
		bool m_twosided;
		int m_numRaysGenerated;
	};
}