#pragma once

#include "Material.h"
#include "Source.h"

namespace material
{
	class Light : public Material
	{
	public:
		Light(Source *emission = new ColorSource(glm::vec3(1.0f))) : m_emission(emission) {}

		virtual glm::vec3 intensity(tracer::Intersection &intersect) const {
			return m_emission->getColor(intersect.texCoord.x, intersect.texCoord.y);
		}

	private:
		Source *m_emission;
	};
}