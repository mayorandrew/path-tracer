#pragma once

#include "Material.h"

namespace material
{
	class Phong : public Material
	{
	public:
		Phong(glm::vec3 diffuse = glm::vec3(1, 1, 1), glm::vec3 specular = glm::vec3(0,0,0), float specularity = 1.0f);

		virtual bool trace(tracer::Intersection &intersect, core::Ray *sample = nullptr) const;
		virtual glm::vec3 blend(tracer::Intersection &intersect, core::Ray &incoming, glm::vec3 value) const;

	private:
		glm::vec3 m_diffuse;
		glm::vec3 m_specular;
		float m_specularity;
	};
}