#pragma once

#include "core/Ray.h"

namespace tracer
{
	class Intersection;
}

namespace material
{
	class Material
	{
	public:
		virtual bool trace(tracer::Intersection &intersect, std::vector<core::Ray> *sample = nullptr, int depth = 0) const {
			return false;
		}

		virtual glm::vec3 intensity(tracer::Intersection &intersect) const {
			return glm::vec3(0.0f);
		}

		virtual glm::vec3 blend(tracer::Intersection &intersect, const std::vector<core::Ray> &incoming, const std::vector<glm::vec3> &value) const {
			return value[0];
		}
	};
}
