#pragma once

#include "Shape.h"

namespace shape
{
	class Triangle
	{
	public:
		int vertices[3];
		int normals[3];
		int texCoords[3];
		int tangents[3];
		int bitangents[3];

		bool intersects(
			const std::vector<glm::vec3> &verticesData,
			const std::vector<glm::vec3> &normalsData,
			const std::vector<glm::vec2> &texCoordsData,
			const std::vector<glm::vec3> &tangentsData,
			const std::vector<glm::vec3> &bitangentsData,
			const core::Ray &ray, 
			tracer::Intersection *intersect = nullptr) const;
	};
}