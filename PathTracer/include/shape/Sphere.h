#pragma once

#include "Shape.h"

namespace shape
{
	class Sphere : public Shape
	{
	public:
		Sphere(float radius = 1.0f);
		virtual ~Sphere();

		virtual bool intersects(const core::Ray &ray, tracer::Intersection *intersect = nullptr) const;

	protected:
		float m_radius;
		float m_sqradius;
	};
}
