#pragma once

#include "Shape.h"

namespace shape
{
	class Plane : public Shape
	{
	public:
		Plane(glm::vec3 pos, glm::vec3 normal);
		virtual ~Plane();

		virtual bool intersects(const core::Ray &ray, tracer::Intersection *intersect = nullptr) const;

	protected:
		glm::vec3 m_pos;
		glm::vec3 m_normal;
	};
}
