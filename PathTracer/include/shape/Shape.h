#pragma once

#include "core/Ray.h"
#include "tracer/Intersection.h"
#include "core/BBox.h"

namespace shape
{
	class Shape
	{
	public:
		Shape() { m_bbox = core::BBox(); }
		virtual ~Shape() {}

		virtual bool intersects(const core::Ray &ray, tracer::Intersection *intersect = nullptr) const {
			return false;
		}

		virtual const core::BBox &getBBox() const {
			return m_bbox;
		}

	protected:
		core::BBox m_bbox;
	};
}
