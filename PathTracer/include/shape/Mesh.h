#pragma once

#include "Shape.h"
#include "Triangle.h"

namespace shape
{
	class Mesh;

	class SubMesh : public Shape {
	public:
		SubMesh();
		virtual ~SubMesh();

		virtual bool intersects(const Mesh &mesh, const core::Ray &ray, tracer::Intersection *intersect = nullptr) const;
		void prepare(const Mesh &mesh);

		std::vector<Triangle> triangles;
	};

	class Mesh : public Shape
	{
	public:
		Mesh();
		virtual ~Mesh();

		virtual bool intersects(const core::Ray &ray, tracer::Intersection *intersect = nullptr) const;
		void prepare();

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec2> texCoords;
		std::vector<glm::vec3> tangents;
		std::vector<glm::vec3> bitangents;
		std::vector<SubMesh> submeshes;
	};
}
