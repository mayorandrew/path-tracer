#pragma once

#include <glm.hpp>
#include "core/Ray.h"
#include "material/Material.h"

namespace tracer
{
	class SceneNode;
	class Intersection
	{
	public:
		core::Ray ray;
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec3 tangent;
		glm::vec3 bitangent;
		glm::vec2 texCoord;
		float distance;
		material::Material *material;
		SceneNode *node;

		void transform(const glm::mat4 &matrix, const glm::mat3 &normalMatrix) {
			ray.selfTransform(matrix);
			position = glm::vec3(matrix * glm::vec4(position, 1.0f));
			normal = glm::normalize(normalMatrix * normal);
			tangent = glm::normalize(glm::vec3(matrix * glm::vec4(tangent, 0.0f)));
			bitangent = glm::normalize(glm::vec3(matrix * glm::vec4(bitangent, 0.0f)));
			distance = glm::length(position - ray.pos());
		}
	};
}
