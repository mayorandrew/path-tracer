#pragma once

#include "tracer/AbstractRenderer.h"

#include "core/Ray.h"
#include "tracer/Camera.h"
#include "tracer/SceneNode.h"

#include "Sampler.h"

#include <random>

namespace tracer
{
	class Renderer : public AbstractRenderer
	{
	public:
		Renderer(int targetWidth, int targetHeight, Camera &cam, SceneNode &scene);
		~Renderer();

		void adjust(Uint32 delta, Uint32 frametime);
		bool render(Sampler &sampler);
		bool renderRays();
		glm::vec3 traceRay(const core::Ray &r, int depth = 0) const;

		inline const core::TImage<float> &varianceMap() const;

	private:
		bool m_adjust;
		int m_pixelSize;
		int m_maxRayDepth;
		int m_pixelsPerStep;
		int m_renderStep;

		SceneNode &m_scene;
		Camera &m_camera;
		core::TImage<float> m_varMap;
	};

	const core::TImage<float> &Renderer::varianceMap() const {
		return m_varMap;
	}
}
