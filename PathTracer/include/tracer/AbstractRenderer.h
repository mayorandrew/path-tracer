#pragma once

#include "core/Image.h"
#include "core/Common.h"

#include "Sampler.h"

namespace tracer
{
	class AbstractRenderer
	{
	public:
		AbstractRenderer(int targetWidth, int targetHeight) :
			m_image(targetWidth, targetHeight), m_sampleData(targetWidth, targetHeight) {
		};

		virtual void adjust(Uint32 delta, Uint32 frametime) = 0;
		virtual bool render(Sampler &sampler) = 0;

		inline const core::Image &image() const;
		inline const core::TImage<glm::vec4> &imageData() const;

		inline Uint32 rgb(float r, float g, float b) const;

	protected:
		core::Image m_image;
		core::TImage<glm::vec4> m_sampleData;
	};



	const core::Image &AbstractRenderer::image() const {
		return m_image;
	}
	const core::TImage<glm::vec4> &AbstractRenderer::imageData() const {
		return m_sampleData;
	}

	Uint32 AbstractRenderer::rgb(float r, float g, float b) const {
		return (core::floorInt(0xFF * (b)) << 16) +
			(core::floorInt(0xFF * (g)) << 8) +
			(core::floorInt(0xFF * (r)));
	}
}