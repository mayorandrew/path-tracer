#pragma once

#include "tracer/AbstractRenderer.h"

#include "core/Ray.h"
#include "tracer/Camera.h"
#include "tracer/SceneNode.h"
#include "tracer/PointLight.h"

#include <random>

namespace tracer
{
	class FastRenderer : public AbstractRenderer
	{
	public:
		FastRenderer(int targetWidth, int targetHeight, Camera &cam, SceneNode &scene, std::vector<tracer::PointLight> &lights);
		~FastRenderer();

		void adjust(Uint32 delta, Uint32 frametime);
		bool render();
		glm::vec3 traceRay(const core::Ray &r, int depth = 0) const;

	private:
		bool m_adjust;
		int m_maxRayDepth;
		int m_pixelsPerStep;
		int m_renderStep;

		SceneNode &m_scene;
		Camera &m_camera;
		std::vector<tracer::PointLight> &m_lights;
	};
}
