#pragma once

#include "shape/Shape.h"
#include "material/Material.h"
#include "tracer/SceneNode.h"
#include "tracer/Camera.h"
#include "tracer/PointLight.h"

namespace tracer
{
	class Scene
	{
	public:
		Scene() {
			root = new tracer::SceneNode();
			camera = nullptr;
		}

		Scene(Scene &&oth) {
			shapes = oth.shapes;
			materials = oth.materials;
			root = oth.root;
			camera = oth.camera;
			lights = oth.lights;
			oth.shapes.clear();
			oth.materials.clear();
			oth.root = nullptr;
			oth.camera = nullptr;
		}

		std::vector<shape::Shape *> shapes;
		std::vector<material::Material *> materials;
		std::vector<PointLight> lights;
		tracer::SceneNode *root;
		tracer::Camera *camera;

		~Scene() {
			if (root) delete root;
			if (camera) delete camera;
			for (auto it : shapes) {
				delete it;
			}
			for (auto it : materials) {
				delete it;
			}
		}
	};
}