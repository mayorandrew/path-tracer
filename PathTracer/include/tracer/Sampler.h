#pragma once

#include "../core/Image.h"

#include <glm.hpp>
#include <random>

namespace tracer
{
	class Sampler
	{
	public:
		Sampler(int width, int height);

		void adjust(const core::TImage<float> &difference);
		glm::ivec2 sample();
		std::vector<glm::ivec2> sampleArray();
		inline void reset();
		inline int num();

	private:
		core::TImage<float> m_map;
		int m_cx, m_cy, m_csample;
		bool m_adjusted;

		std::uniform_real_distribution<float> m_xDistrib;
		std::uniform_real_distribution<float> m_yDistrib;
	};



	void Sampler::reset() {
		m_cx = m_cy = m_csample = 0;
	}

	int Sampler::num() {
		return 100000;
	}
}