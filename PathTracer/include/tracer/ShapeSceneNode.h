#pragma once
#include "SceneNode.h"

#include "shape/Shape.h"
#include "material/Material.h"

namespace tracer
{
	class ShapeSceneNode :
		public SceneNode
	{
		friend MyFormatEncoder;
	public:
		ShapeSceneNode(const shape::Shape &shape, material::Material &material, SceneNode *parent = nullptr);
		virtual ~ShapeSceneNode();

		virtual void evaluateMatrix();
		virtual bool intersects(const core::Ray &ray, Intersection *intersect = nullptr);

	protected:
		const shape::Shape &m_shape;
		material::Material &m_material;
	};
}
