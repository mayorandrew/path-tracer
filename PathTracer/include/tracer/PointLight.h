#pragma once

#include <glm.hpp>

namespace tracer
{
	class PointLight
	{
	public:
		glm::vec3 position;
		glm::vec3 color;

		PointLight(glm::vec3 pos = glm::vec3(), glm::vec3 col = glm::vec3()) : position(pos), color(col) {}
	};
}