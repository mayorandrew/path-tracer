#pragma once

#include "core/Ray.h"

#include <glm.hpp>

class MyFormatEncoder;

namespace tracer
{
	class Camera
	{
		friend MyFormatEncoder;
	public:
		Camera(int width, int height, float angle, glm::vec3 up = glm::vec3(0, 1, 0), glm::vec3 dir = glm::vec3(0, 0, 1), glm::vec3 pos = glm::vec3(0));
		~Camera();

		inline void setPosition(glm::vec3 pos);
		inline void setDirection(glm::vec3 dir);
		inline void lookAt(glm::vec3 target);

		inline glm::vec3 position() const;
		inline glm::vec3 direction() const;

		inline void setDimensions(int width, int height);

		core::Ray ray(float x, float y) const;

	private:
		void _update();

		glm::vec3 m_up;
		glm::vec3 m_position;
		glm::vec3 m_direction;
		float m_angleH, m_angleV;

		glm::vec3 m_left, m_top, m_corner;

		int m_width, m_height;
		float m_stepX, m_stepY;
	};

	glm::vec3 Camera::position() const {
		return m_position;
	}

	glm::vec3 Camera::direction() const {
		return m_direction;
	}

	void Camera::setPosition(glm::vec3 pos) {
		m_position = pos;
		_update();
	}

	void Camera::setDirection(glm::vec3 dir) {
		m_direction = glm::normalize(dir);
		_update();
	}

	void Camera::lookAt(glm::vec3 target) {
		m_direction = glm::normalize(target - m_position);
		_update();
	}

	void Camera::setDimensions(int width, int height) {
		m_width = width; m_height = height;
		_update();
	}
}