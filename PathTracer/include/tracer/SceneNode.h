#pragma once

#include "core/Ray.h"
#include "Intersection.h"
#include "core/BBox.h"

#include <glm.hpp>
#include <gtc/quaternion.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/matrix_inverse.hpp>
#include <vector>

class MyFormatEncoder;

namespace tracer
{
	class SceneNode
	{
		friend MyFormatEncoder;
	public:
		SceneNode(SceneNode *parent = nullptr);
		virtual ~SceneNode();

		virtual bool intersects(const core::Ray &ray, Intersection *intersect = nullptr);
		virtual void evaluateMatrix();
		virtual void evaluate();
		virtual void add(SceneNode *child);
		const core::BBox &getBBox() const { return m_bbox; }

		void setMatrix(const glm::mat4 &modelmatrix);
		void setRelMatrix(const glm::mat4 &modelmatrix);
		const glm::mat4 &getMatrix() const { return m_modelmatrix;  };
		std::string name;

	protected:
		glm::mat4 m_relmodelmatrix;
		glm::mat4 m_modelmatrix;
		glm::mat4 m_invmodelmatrix;
		glm::mat3 m_normalmatrix;

		SceneNode *m_parent;
		std::vector<SceneNode *> m_children;

		core::BBox m_bbox;
	};
}
