#pragma once

#include <glm.hpp>

#include "Common.h"

namespace core
{
	class Ray
	{
	public:
		inline Ray(glm::vec3 pos = glm::vec3(), glm::vec3 dir = glm::vec3(1.0f));
		inline ~Ray();

		inline Ray transform(const glm::mat4 &mat) const;
		inline void selfTransform(const glm::mat4 &mat);

		glm::vec3 pos() const { return m_pos; };
		glm::vec3 dir() const { return m_dir; };

	private:
		glm::vec3 m_pos;
		glm::vec3 m_dir;
	};



	Ray::Ray(glm::vec3 pos, glm::vec3 dir) : m_pos(pos) {
		m_dir = glm::normalize(dir);
	}

	Ray::~Ray() {
	}

	Ray Ray::transform(const glm::mat4 &mat) const {
		return Ray(
			glm::vec3(mat * glm::vec4(m_pos, 1.0f)),
			glm::vec3(mat * glm::vec4(m_dir, 0.0f))
		);
	}

	void Ray::selfTransform(const glm::mat4 &mat) {
		m_pos = glm::vec3(mat * glm::vec4(m_pos, 1.0f));
		m_dir = glm::normalize(glm::vec3(mat * glm::vec4(m_dir, 0.0f)));
	}
}
