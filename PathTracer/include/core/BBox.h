#pragma once

#include "core/Ray.h"
#include <glm.hpp>

namespace core
{
	class BBox
	{
	public:
		BBox();
		BBox(glm::vec3 bottom_, glm::vec3 top_);

		void add(glm::vec3 point);
		void add(const BBox &bbox);
		BBox transform(glm::mat4 model) const;
		bool intersects(const core::Ray &ray) const;

		glm::vec3 bottom;
		glm::vec3 top;

	private:
		bool m_empty;
	};
}