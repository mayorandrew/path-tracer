#pragma once

#include <string>

namespace core
{

	class Exception
	{
	public:
		Exception(const std::string &msg_) : msg(msg_) {}
		virtual ~Exception() {}

		virtual std::string toString() const { return msg; }

		std::string msg;
	};

}

