#pragma once
#include <SDL.h>
#include <assert.h>

namespace core
{
	template<class T>
	class TImage
	{
	public:
		TImage(int width, int height);
		TImage(const TImage &im);
		TImage(TImage &&im);
		~TImage();

		inline int width() const;
		inline int height() const;

		inline T &operator[] (int i);
		inline const T &operator[] (int i) const;
		inline T &pixel(int x, int y);
		inline const T &pixel(int x, int y) const;
		inline const T &cpixel(int x, int y) const;

		inline T *data();
		inline const T *data() const;

		T max() const;
		T sum() const;
		void operator*= (T val);
		void operator+= (T val);
		void operator+= (const TImage &val);
		void operator= (const TImage &val);

	private:
		int m_width;
		int m_height;
		T *m_data;
	};



	template<class T>
	TImage<T>::TImage(int width, int height) : m_width(width), m_height(height) {
		m_data = new T[width * height];
		for (int i = 0; i < width * height; i++) {
			m_data[i] = T();
		}
	}

	template<class T>
	TImage<T>::TImage(const TImage &oth) {
		m_width = oth.m_width; m_height = oth.m_height;
		m_data = new T[m_width * m_height];
		SDL_memcpy(m_data, oth.m_data, sizeof(*m_data) * m_width * m_height);
	}

	template<class T>
	TImage<T>::TImage(TImage &&oth) {
		m_width = oth.m_width; m_height = oth.m_height;
		m_data = oth.m_data;
		oth.m_data = nullptr;
		oth.m_width = oth.m_height = 0;
	}

	template<class T>
	TImage<T>::~TImage() {
		if (m_data) delete[] m_data;
	}

	template<class T>
	void TImage<T>::operator =(const TImage<T> &oth) {
		if (m_width == oth.m_width && m_height == oth.m_height) {
			for (int i = 0; i < m_width * m_height; i++) {
				m_data[i] = oth.m_data[i];
			}
		} else {
			delete[] m_data;
			m_width = oth.m_width;
			m_height = oth.m_height;
			m_data = new T[m_width * m_height];
		}
	}

	template<class T>
	int TImage<T>::width() const {
		return m_width;
	}

	template<class T>
	int TImage<T>::height() const {
		return m_height;
	}

	template<class T>
	T &TImage<T>::operator[] (int i) {
		assert(i >= 0 && i < m_width * m_height);
		return m_data[i];
	}

	template<class T>
	const T &TImage<T>::operator[] (int i) const {
		assert(i >= 0 && i < m_width * m_height);
		return m_data[i];
	}

	template<class T>
	T &TImage<T>::pixel(int x, int y) {
		if (x >= 0 && x < m_width && y >= 0 && y < m_height) {
			return m_data[y * m_width + x];
		} else {
			return m_data[0];
		}
	}

	template<class T>
	const T &TImage<T>::pixel(int x, int y) const {
		if (x >= 0 && x < m_width && y >= 0 && y < m_height) {
			return m_data[y * m_width + x];
		} else {
			return m_data[0];
		}
	}

	template<class T>
	const T &TImage<T>::cpixel(int x, int y) const {
		x = x % m_width;
		y = y % m_height;
		if (x < 0) { x += m_width; }
		if (y < 0) { y += m_height; }

		return m_data[y * m_width + x];
	}

	template<class T>
	T *TImage<T>::data() {
		return m_data;
	}

	template<class T>
	const T *TImage<T>::data() const {
		return m_data;
	}

	template<class T>
	void TImage<T>::operator*=(T val) {
		int size = m_width * m_height;
		for (int i = 0; i < size; i++) {
			m_data[i] *= val;
		}
	}

	template<class T>
	void TImage<T>::operator+=(T val) {
		int size = m_width * m_height;
		for (int i = 0; i < size; i++) {
			m_data[i] += val;
		}
	}

	template<class T>
	T TImage<T>::max() const {
		T max = T();
		int size = m_width * m_height;
		for (int i = 0; i < size; i++) {
			if (max < m_data[i]) {
				max = m_data[i];
			}
		}
		return max;
	}

	template<class T>
	T TImage<T>::sum() const {
		T sum = T();
		int size = m_width * m_height;
		for (int i = 0; i < size; i++) {
			sum += m_data[i];
		}
		return sum;
	}

	typedef TImage<Uint32> Image;

}