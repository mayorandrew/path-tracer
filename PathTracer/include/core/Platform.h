#pragma once

#include "Image.h"

#include <SDL.h>
#include <functional>
#include <glm.hpp>

namespace core
{
	class Platform
	{
	public:
		Platform();
		~Platform();

		void initTexture(int width, int height);
		void updateTexture(int i, const Image &img);
		void updateTexture(int i, const TImage<float> &img);

		void render();
		void mainloop(std::function<void(Uint32, Uint32)> frame);
		void saveImage(const TImage<glm::vec4> &image) const;

		static TImage<glm::vec3> loadTexture(const std::string &path);
		static Uint32 getPixel(SDL_Surface *surface, int x, int y);

	private:
		SDL_Window *m_window;
		SDL_Renderer *m_renderer;
		SDL_Texture **m_texture;
		int m_numTextures;
		int m_width;
		int m_height;
	};
}

