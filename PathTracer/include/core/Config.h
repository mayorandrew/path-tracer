#pragma once

#include <yaml-cpp/yaml.h>
#include <string>
#include <glm.hpp>

namespace core
{
	class Config
	{
	public:
		static Config *getSingleton();
		void readFile(const std::string &filename);

		template <class T>
		T var(const char *varname);

		template <>
		glm::vec3 var<glm::vec3>(const char *varname);

	private:
		Config();
		~Config();

		YAML::Node m_data;
		static Config *m_config;
	};



	template <class T>
	T Config::var(const char *varname) {
		if (m_data[varname]) {
			return m_data[varname].as<T>();
		}
		return T();
	}

	template <>
	glm::vec3 Config::var<glm::vec3>(const char *varname) {
		if (m_data[varname]) {
			YAML::Node &n = m_data[varname];
			return glm::vec3(n[0].as<float>(), n[1].as<float>(), n[2].as<float>());
		}
		return glm::vec3();
	}
}