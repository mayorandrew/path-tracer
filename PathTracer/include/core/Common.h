#pragma once

#include <glm.hpp>
#include <gtc/matrix_inverse.hpp>
#include <random>

#include "Image.h"
#include "core\Config.h"

#ifndef M_PI
#define M_PI 3.14159265359f
#define M_EPS 0.000006f
#endif

namespace core
{
	inline int floorInt(float t) {
		return (int)floor(t);
	};

	/*template <class T>
	inline T min(T a, T b) {
		return a < b ? a : b;
	}
	
	template <class T>
	inline T max(T a, T b) {
		return a > b ? a : b;
	}*/

	inline glm::vec3 transformNormal(glm::vec3 deltanormal, glm::vec3 tangent, glm::vec3 normal, glm::vec3 bitangent) {
		if (deltanormal != glm::vec3(0, 1, 0)) {
			glm::mat3 tbn = glm::inverseTranspose(glm::mat3(bitangent, normal, tangent));
			normal = glm::normalize(tbn * deltanormal);
		} else {
			normal = normal;
		}
		return normal;
	}

	inline glm::vec3 anyPerpendicular(glm::vec3 vec) {
		if (abs(vec.y) > 0.2f || abs(vec.z) > 0.2f) {
			return glm::cross(vec, glm::vec3(1, 0, 0));
		} else {
			return glm::cross(vec, glm::vec3(0, 1, 0));
		}
	}

	inline glm::vec3 gammaCorrect(glm::vec3 value) {
		float exponent = Config::getSingleton()->var<float>("gamma");
		return glm::vec3(pow(value.x, 1 / exponent), pow(value.y, 1 / exponent), pow(value.z, 1 / exponent));
	}


	inline glm::ivec3 unpackColor(Uint32 color) {
		return glm::ivec3(
			color & 0xFF,
			(color >> 8) & 0xFF,
			(color >> 16) & 0xFF
			);
	}

	inline Uint32 packColor(glm::ivec3 color) {
		return (color[0] & 0xFF) + ((color[1] & 0xFF) << 8) + ((color[2] & 0xFF) << 16);
	}

	inline void imageDifference(TImage<float> &result, const Image &a, const Image &b) {
		int size = result.width() * result.height();
		assert(size == a.width() * a.height() && size == b.width() * b.height());
		for (int i = 0; i < size; i++) {
			glm::ivec3 aC = unpackColor(a[i]);
			glm::ivec3 bC = unpackColor(b[i]);
			result[i] = glm::length(glm::vec3(aC) - glm::vec3(bC));
		}
	}

	inline Image convertImage(const TImage<float> &image) {
		Image res(image.width(), image.height());
		int size = image.width() * image.height();
		float max = image.max();
		for (int i = 0; i < size; i++) {
			res[i] = Uint32(image[i] * 255.f / max);
		}
		return res;
	}

	extern std::default_random_engine g_generator;
}