#pragma once

#include "tracer/Scene.h"
#include "core/Image.h"

#include <assimp/scene.h>

#include <map>

namespace decode
{
	class AiDecoder
	{
	public:
		tracer::Scene load(const std::string &filepath);

	private:
		void _convertAiNodeToSceneNode(aiNode *sNode, tracer::SceneNode *tNode);
		core::TImage<glm::vec3> _convertHeightmapToNormalmap(const core::TImage<glm::vec3> &heightmap, float scale = 1.0f);

		std::map<std::string, tracer::SceneNode *> m_registry;

		tracer::Scene *m_scene;
		const aiScene *aiscene;
	};
}