#pragma once

#include "shape/Mesh.h"
#include <string>

namespace decode
{
	class MeshDecoder
	{
	public:
		static shape::Mesh load(const std::string &filename);
	};
}